function love.load()
    logo = love.graphics.newImage("assets/logo.png")
    love.graphics.setBackgroundColor(1, 1, 1, 1)
end

currentTime = 0

opacity = 0.0
animationTime = 1

function easeOutBack(x)
    c1 = 5.70158
    c3 = c1 + 1

    return 1 + c3 * (x - 1) ^ 3 + c1 * (x - 1) ^ 2
end

function love.update(dt)
    currentTime = currentTime + dt

    x = currentTime / animationTime

    if x > 1.0 then x = 1.0 end

    opacity = easeOutBack(x) / 1.6

    if opacity > 1.0 then opacity = 1.0 end
end

function love.draw()
    love.graphics.setColor(1, 1, 1, opacity)
    love.graphics.draw(logo, 0, 0)
end